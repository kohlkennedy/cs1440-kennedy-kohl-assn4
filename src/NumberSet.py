import random
import sys

class NumberSet():
    def __init__(self, size):
        """NumberSet constructor"""
        self.__m_size = size
        self.__m_numberSet = []


        for i in range(self.__m_size):
            self.__m_numberSet.append(i + 1)

    def getSize(self):
        """Return an integer: the size of the NumberSet"""
        return len(self.__m_numberSet)

    def get(self, index):
        """Return an integer: get the number from this NumberSet at an index"""
        if len(self.__m_numberSet) == 0:
            return None
        else:
            return self.__m_numberSet[index]


    def randomize(self):
        """void function: Shuffle this NumberSet"""
        random.shuffle(self.__m_numberSet)


    def getNext(self):
        """Return an integer: when called repeatedly return successive values
        from the NumberSet until the end is reached, at which time 'None' is returned"""
        if len(self.__m_numberSet) == 0:
            return None
        else:
            return self.__m_numberSet.pop(0)
