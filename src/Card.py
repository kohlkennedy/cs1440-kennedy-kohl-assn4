import sys

import NumberSet

class Card():
    def __init__(self, idnum, size, numberSet):
        """Card constructor"""
        self.__m_idnum = idnum
        self.__m_size = size
        self.__m_numberSet = numberSet
        self.__m_card = []

        
        if self.__m_size % 2 == 1 :
            __m_midpoint = (self.__m_size - 1) // 2
        else:
            __m_midpoint = -1

        __m_x_coordinate,__m_y_coordinate = self.__m_size,self.__m_size

        for i in range(__m_x_coordinate):
            temp = []
            for j in range(__m_y_coordinate):
                temp.append(None)
            self.__m_card.append(temp)

        self.__m_card_string = ("+-----" * self.__m_size) + "+\n"

        for i in range(__m_y_coordinate):
            for j in range(__m_x_coordinate):
                if i == __m_midpoint and j == __m_midpoint:
                    self.__m_card_string += "|Free!"
                    self.__m_card[i][j] = -1
                else:
                    number = self.__m_numberSet.getNext()
                    self.__m_card[i][j] = number

                    self.__m_card_string += "|" + "{:^5d}".format(number)
                    if j == __m_x_coordinate - 1:
                        self.__m_card_string += "|\n" + ("+-----" * self.__m_size) + "+\n"



    def getId(self):
        """Return an integer: the ID number of the card"""
        return self.__m_idnum

    def getSize(self):
        """Return an integer: the size of one dimension of the card.
        A 3x3 card will return 3, a 5x5 card will return 5, etc.
        """
        return self.__m_size

    def print(self, file=sys.stdout):
        """void function:
        Prints a card to the screen or to an open file object"""
        print(self.__m_card_string, file=file)
        